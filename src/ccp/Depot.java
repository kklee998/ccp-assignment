/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccp;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author kklee
 */
public class Depot {

    private List<Bus> busOnRamp;
    private List<Bus> entranceArea;
    private List<Bus> exitArea;
    private List<MBay> mBayQ;
    private List<CBay> cBayQ;
    private static int parkingSpace = 10;
    private Bus busAtQueue;
    private Bus busAtRamp;
    Bus busAtEntrance;
    Bus busAtExit;
    private Spawn spawn;
    MBay mbay;
    CBay cbay;
    int busIn = 0;
    int busOut = 0;
    boolean closingTime = false;

    public Depot(Spawn spawn) {
        busOnRamp = new LinkedList<>();
        entranceArea = new LinkedList<>();
        exitArea = new LinkedList<>();
        mBayQ = new LinkedList<>();
        cBayQ = new LinkedList<>();
        this.spawn = spawn;
    }

    void depotStats() {
        
        //statistics

        System.out.println("Number of buses that enter depot: " + busIn);
        System.out.println("Number of buses that exit depot: " + busOut);
    }

    synchronized void enterRamp() throws InterruptedException {

        while (getSpawn().getBusQueue().isEmpty()) {
            return;
        }
        if (getEntranceArea().size() == getParkingSpace()) {
            System.out.println("\nEntrance is currently full!\n");
            return;
            //prevents bus from occupying ramp if entrance area is full
        }
        setBusAtQueue(((LinkedList<Bus>) getSpawn().getBusQueue()).poll());
        System.out.println(getBusAtQueue().getName() + " is entering to the "
                + "ramp");
        ((LinkedList<Bus>) getBusOnRamp()).offer(getBusAtQueue());
        System.out.println(getBusAtQueue().getName() + " is entering ON the "
                + "ramp");
        Thread.sleep(100 * getBusAtQueue().getSize());

    }

    synchronized void exitRamp(Bus bus) throws InterruptedException {

        System.out.println(bus.getName() + "is exiting to the ramp");
        ((LinkedList<Bus>) getBusOnRamp()).offer(bus);
        System.out.println(bus.getName() + "is exiting ON the ramp");
        Thread.sleep(100 * bus.getSize());
        bus = ((LinkedList<Bus>) getBusOnRamp()).poll();
        System.out.println(bus.getName() + " has exited the depot");
        busOut += 1;

    }

    synchronized void enterEntrance() throws InterruptedException {

        while (!busOnRamp.isEmpty()) {

            setBusAtRamp(((LinkedList<Bus>) getBusOnRamp()).poll());
            System.out.println(getBusAtRamp().getName() + " has entered the entrance");
            ((LinkedList<Bus>) getEntranceArea()).offer(getBusAtRamp());
            busIn += 1;

        }

    }

    synchronized void enterExit(Bus bus) throws InterruptedException {

        if (getExitArea().size() == getParkingSpace()) {
            System.out.println("\nExit is currently full!\n");
            return;
        }
        ((LinkedList<Bus>) getExitArea()).offer(bus);
        System.out.println(bus.getName() + " is now in the exit area");
        busAtExit = ((LinkedList<Bus>) getExitArea()).poll();
        if (busAtExit == null) {
            return;
        }
        exitRamp(busAtExit);

    }

    synchronized void enterMechanicBay() throws InterruptedException {

        while (!entranceArea.isEmpty()) {

            busAtEntrance = ((LinkedList<Bus>) getEntranceArea()).peek();
            mbay = ((LinkedList<MBay>) mBayQ).peek();
            if (mbay == null || busAtEntrance == null) {
                return;
            }

            if (busAtEntrance.isNeedRepair() || busAtEntrance.isNeedExtraRepair()) {
                busAtEntrance = ((LinkedList<Bus>) getEntranceArea()).poll();
                mbay = ((LinkedList<MBay>) mBayQ).poll();
                
                //normal bus

                if (busAtEntrance.getSize() > 1) {
                    mbay.addToBusInMBay(busAtEntrance);

                } else {
                    //Minibus
                    mbay.addToBusInMBay(busAtEntrance);
                    busAtEntrance = ((LinkedList<Bus>) getEntranceArea()).peek();
                    if (busAtEntrance == null) {

                        return;
                    }
                    if (busAtEntrance.getSize() == 1) {
                        if (busAtEntrance.isNeedRepair() || busAtEntrance.isNeedExtraRepair()) {
                            busAtEntrance = ((LinkedList<Bus>) getEntranceArea()).poll();
                            mbay.addToBusInMBay(busAtEntrance);
                        } else {
                            return;
                        }

                    }
                }//minibus ENDELSE
            } else {
                
                //if and only if the bus needs ONLY cleaning
                Bus bus = ((LinkedList<Bus>) getEntranceArea()).poll();
                cbay = ((LinkedList<CBay>) cBayQ).peek();
                if (cbay == null) {
                    return;
                }
                enterCleanerBay(bus);
            }

        }

    }

    synchronized void enterCleanerBay(Bus bus) throws InterruptedException {

        cbay = ((LinkedList<CBay>) cBayQ).poll();

        if (cbay == null) {
            return;
        }

        if (bus.getSize() > 1) {
            cbay.addToBusInCBay(bus);

        } else {
            //Minibus
            cbay.addToBusInCBay(bus);
            bus = ((LinkedList<Bus>) getEntranceArea()).peek();
            if (bus == null) {
                return;
            }
            if (bus.getSize() == 1) {
                if (bus.isNeedRepair() || bus.isNeedExtraRepair()) {
                    return;
                } else {
                    bus = ((LinkedList<Bus>) getEntranceArea()).poll();
                    cbay.addToBusInCBay(bus);
                }

            }
        }

    }

    /**
     * @return the busOnRamp
     */
    public List<Bus> getBusOnRamp() {
        return busOnRamp;
    }

    /**
     * @param busOnRamp the busOnRamp to set
     */
    public void setBusOnRamp(List<Bus> busOnRamp) {
        this.busOnRamp = busOnRamp;
    }

    /**
     * @return the entranceArea
     */
    public List<Bus> getEntranceArea() {
        return entranceArea;
    }

    /**
     * @param entranceArea the entranceArea to set
     */
    public void setEntranceArea(List<Bus> entranceArea) {
        this.entranceArea = entranceArea;
    }

    /**
     * @return the exitArea
     */
    public List<Bus> getExitArea() {
        return exitArea;
    }

    /**
     * @param exitArea the exitArea to set
     */
    public void setExitArea(List<Bus> exitArea) {
        this.exitArea = exitArea;
    }

    /**
     * @return the parkingSpace
     */
    public static int getParkingSpace() {
        return parkingSpace;
    }

    /**
     * @param aParkingSpace the parkingSpace to set
     */
    public static void setParkingSpace(int aParkingSpace) {
        parkingSpace = aParkingSpace;
    }

    /**
     * @return the busAtQueue
     */
    public Bus getBusAtQueue() {
        return busAtQueue;
    }

    /**
     * @param busAtQueue the busAtQueue to set
     */
    public void setBusAtQueue(Bus busAtQueue) {
        this.busAtQueue = busAtQueue;
    }

    /**
     * @return the busAtRamp
     */
    public Bus getBusAtRamp() {
        return busAtRamp;
    }

    /**
     * @param busAtRamp the busAtRamp to set
     */
    public void setBusAtRamp(Bus busAtRamp) {
        this.busAtRamp = busAtRamp;
    }

    /**
     * @return the spawn
     */
    public Spawn getSpawn() {
        return spawn;
    }

    /**
     * @param spawn the spawn to set
     */
    public void setSpawn(Spawn spawn) {
        this.spawn = spawn;
    }

    public List<MBay> getmBayQ() {
        return mBayQ;
    }

    public void setmBayQ(List<MBay> mBayQ) {
        this.mBayQ = mBayQ;
    }

    public List<CBay> getcBayQ() {
        return cBayQ;
    }

    public void setcBayQ(List<CBay> cBayQ) {
        this.cBayQ = cBayQ;
    }

    public MBay getMbay() {
        return mbay;
    }

    public void setMbay(MBay mbay) {
        this.mbay = mbay;
    }

    public CBay getCbay() {
        return cbay;
    }

    public void setCbay(CBay cbay) {
        this.cbay = cbay;
    }
    
    public synchronized void setClosingTime() {
        this.closingTime = true;
    }
}
