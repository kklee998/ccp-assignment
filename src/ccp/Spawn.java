/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccp;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author kklee
 */
public class Spawn extends Thread {

    private int busID = 0;
    private int busCounter = 0;
    private int numberOfBus;
    private List<Bus> busQueue;
    private volatile boolean closingTime = false;
    private volatile boolean beforeClosingTime = false;
    private boolean rain;
    private Random rand = new Random();

    public Spawn(int numberOfBus, boolean rain) {
        this.numberOfBus = numberOfBus;
        busQueue = new LinkedList<>();
        this.rain = rain;
    }

    @Override
    public void run() {

        this.setName("Spawn-Thread");
        
        while (!isBeforeClosingTime() && getNumberOfBus() > getBusCounter()) {
            Bus bus = new Bus(this);
            boolean rand_clean = getRand().nextBoolean();
            boolean rand_repair = getRand().nextBoolean();
            boolean rand_extra = getRand().nextBoolean();
            bus.setNeedRepair(rand_repair);
            bus.setNeedExtraRepair(rand_extra);
            if (isRain() == false) {
                bus.setNeedClean(rand_clean);

            }
            bus.setName("BUS" + getBusID());
            Thread thbus = new Thread(bus);
            thbus.start();
            try {

                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.err.println(e);
            }
            setBusCounter(getBusCounter() + 1);
            setBusID(getBusID() + 1);
        }

        while (!closingTime) {
            Iterator iter = busQueue.listIterator(0);
            
            //clear backlog of bus  
            while (iter.hasNext()) {
                System.out.println(((LinkedList<Bus>) busQueue).poll().getName()
                        + " We have close, please come back tomorrow!\n");

            }

            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    public void addToQueue(Bus bus) {
        
        
        //reject buses that are perfect condition
        if (bus.isNeedClean() || bus.isNeedRepair() || bus.isNeedExtraRepair()) {
            ((LinkedList<Bus>) getBusQueue()).offer(bus);
            System.out.println(bus.getName() + " is waiting in queue");
            if (bus.isNeedClean()) {
                System.out.println(bus.getName() + " needs to be cleaned");
            }
            if (bus.isNeedRepair()) {
                System.out.println(bus.getName() + " needs to be repaired");
            }
            if (bus.isNeedExtraRepair()) {
                System.out.println(bus.getName() + " needs to be SUPER repaired");
            }
//            synchronized (getBusQueue()) {
//                getBusQueue().notifyAll();
//            }

        } else {
            System.out.println(bus.getName() + " is perfectly fine and will"
                    + " now return home");
        }

    }

    public synchronized void setClosingTime() {
        setClosingTime(true);
        System.out.println("\nWE HAVE CLOSED! NO MORE BUS\n");
    }

    public synchronized void setBeforeClosingTime() {
        setBeforeClosingTime(true);
        System.out.println("\nWE ARE DONE FOR TODAY!\n");
    }

    /**
     * @return the busID
     */
    public int getBusID() {
        return busID;
    }

    /**
     * @param busID the busID to set
     */
    public void setBusID(int busID) {
        this.busID = busID;
    }

    /**
     * @return the busCounter
     */
    public int getBusCounter() {
        return busCounter;
    }

    /**
     * @param busCounter the busCounter to set
     */
    public void setBusCounter(int busCounter) {
        this.busCounter = busCounter;
    }

    /**
     * @return the numberOfBus
     */
    public int getNumberOfBus() {
        return numberOfBus;
    }

    /**
     * @param numberOfBus the numberOfBus to set
     */
    public void setNumberOfBus(int numberOfBus) {
        this.numberOfBus = numberOfBus;
    }

    /**
     * @return the busQueue
     */
    public List<Bus> getBusQueue() {
        return busQueue;
    }

    /**
     * @param busQueue the busQueue to set
     */
    public void setBusQueue(List<Bus> busQueue) {
        this.busQueue = busQueue;
    }

    /**
     * @return the closingTime
     */
    public boolean isClosingTime() {
        return closingTime;
    }

    /**
     * @param closingTime the closingTime to set
     */
    public void setClosingTime(boolean closingTime) {
        this.closingTime = closingTime;
    }

    /**
     * @return the beforeClosingTime
     */
    public boolean isBeforeClosingTime() {
        return beforeClosingTime;
    }

    /**
     * @param beforeClosingTime the beforeClosingTime to set
     */
    public void setBeforeClosingTime(boolean beforeClosingTime) {
        this.beforeClosingTime = beforeClosingTime;
    }

    /**
     * @return the rain
     */
    public boolean isRain() {
        return rain;
    }

    /**
     * @param rain the rain to set
     */
    public void setRain(boolean rain) {
        this.rain = rain;
    }

    /**
     * @return the rand
     */
    public Random getRand() {
        return rand;
    }

    /**
     * @param rand the rand to set
     */
    public void setRand(Random rand) {
        this.rand = rand;
    }

}
