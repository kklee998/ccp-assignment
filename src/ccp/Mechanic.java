/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccp;

/**
 *
 * @author kklee
 */
public class Mechanic extends Thread {

    private Depot depot;
    private MBay mbay;

    private volatile boolean atWork = true;

    public Mechanic(MBay mbay, Depot depot) {
        this.mbay = mbay;
        this.depot = depot;

    }

    @Override
    public void run() {
        this.setName("Mechanic " + this.getId());
        try {
            while (!isAtWork()) {
                getMbay().fixBus(this);

                this.sleep(50);
            }
            this.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void goHome() {
        setAtWork(false);
        System.out.println("Mechanics will go home now");

    }

    /**
     * @return the depot
     */
    public Depot getDepot() {
        return depot;
    }

    /**
     * @param depot the depot to set
     */
    public void setDepot(Depot depot) {
        this.depot = depot;
    }

    /**
     * @return the mbay
     */
    public MBay getMbay() {
        return mbay;
    }

    /**
     * @param mbay the mbay to set
     */
    public void setMbay(MBay mbay) {
        this.mbay = mbay;
    }

    /**
     * @return the atWork
     */
    public boolean isAtWork() {
        return depot.closingTime;
    }

    /**
     * @param atWork the atWork to set
     */
    public void setAtWork(boolean atWork) {
        this.atWork = atWork;
    }

}
