/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccp;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author kklee
 */
public class CBay implements Runnable {

    private String cBayName;
    private Bus bus;
    private List<Bus> busInCBay;
    private Bay bay;
    private Depot depot;
    int cleaningTime = 500;

    public CBay(Depot depot) {
        busInCBay = new LinkedList<>();
        this.depot = depot;

    }
    
    public void run() {
        Cleaner c;
        c = new Cleaner(this, depot);
        c.start();
    }

    synchronized public void addToBusInCBay(Bus bus) {

        synchronized (busInCBay) {
            ((LinkedList<Bus>) getBusInCBay()).offer(bus);
            System.out.println("\n" + bus.getName() + " is added into cleaning bay: "
                    + getcBayName() + "\n");

            //same issue with MBay, unable to notify
        }
    }

    public void cleanBus(Cleaner clean) {
        synchronized (busInCBay) {
            bus = ((LinkedList<Bus>) busInCBay).poll();
        }
        try {

            if (bus != null) {
                System.out.println(bus.getName() + " is now being cleaned by "
                        + clean.getName() + " in "
                        + this.getcBayName());
                bus.setNeedClean(false);
                Thread.sleep(cleaningTime * bus.getSize());
                System.out.println("\n" + bus.getName() + " CLEANING DONE!\n");
                depot.enterExit(bus);
            }//END ITER

            synchronized (depot.getcBayQ()) {
                ((LinkedList) depot.getcBayQ()).offer(this);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public String getcBayName() {
        return cBayName;
    }

    public void setcBayName(String cBayName) {
        this.cBayName = cBayName;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public List<Bus> getBusInCBay() {
        return busInCBay;
    }

    public void setBusInCBay(List<Bus> busInCBay) {
        this.busInCBay = busInCBay;
    }

    public Bay getBay() {
        return bay;
    }

    public void setBay(Bay bay) {
        this.bay = bay;
    }

    public Depot getDepot() {
        return depot;
    }

    public void setDepot(Depot depot) {
        this.depot = depot;
    }

}
