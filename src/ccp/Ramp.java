/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccp;

/**
 *
 * @author kklee
 */
class Ramp extends Thread {

    private volatile boolean beforeClosingTime;
    private volatile boolean closingTime = false;
    private Depot depot;

    public Ramp(Depot depot) {
        this.beforeClosingTime = false;
        this.depot = depot;
    }

    @Override
    public void run() {
        this.setName("Ramp-Thread");
        while (!isClosingTime()) {
            while (!isBeforeClosingTime()) {
                try {
                    getDepot().enterRamp();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        }

    }

    public synchronized void setBeforeClosingTime() {
        setBeforeClosingTime(true);
        System.out.println("\nNo more entering through ramp!\n");
    }

    public synchronized void setClosingTime() {
        setClosingTime(true);
        System.out.println("\nRamp is closing!\n");
    }

    /**
     * @return the beforeClosingTime
     */
    public boolean isBeforeClosingTime() {
        return beforeClosingTime;
    }

    /**
     * @param beforeClosingTime the beforeClosingTime to set
     */
    public void setBeforeClosingTime(boolean beforeClosingTime) {
        this.beforeClosingTime = beforeClosingTime;
    }

    /**
     * @return the closingTime
     */
    public boolean isClosingTime() {
        return closingTime;
    }

    /**
     * @param closingTime the closingTime to set
     */
    public void setClosingTime(boolean closingTime) {
        this.closingTime = closingTime;
    }

    /**
     * @return the depot
     */
    public Depot getDepot() {
        return depot;
    }

    /**
     * @param depot the depot to set
     */
    public void setDepot(Depot depot) {
        this.depot = depot;
    }

}
