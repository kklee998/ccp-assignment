/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccp;

/**
 *
 * @author kklee
 */
public class Bus implements Runnable {

    private String name;
    private int size = 2;
    private boolean needRepair;
    private boolean needClean = false;
    private boolean needExtraRepair;
    private long busTimeIn;
    private long busTimeOut;


    private Spawn spawn;
    // https://stackoverflow.com/questions/16418571/what-is-the-use-of-encapsulation-when-im-able-to-change-the-property-values-wit

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNeedRepair() {

        return needRepair;
    }

    public void setNeedRepair(boolean needRepair) {
        this.needRepair = needRepair;
    }

    public boolean isNeedClean() {

        return needClean;
    }

    public void setNeedClean(boolean needClean) {
        this.needClean = needClean;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isNeedExtraRepair() {

        return needExtraRepair;
    }

    public void setNeedExtraRepair(boolean needExtraRepair) {
        this.needExtraRepair = needExtraRepair;
    }

    public Spawn getSpawn() {
        return spawn;
    }

    public void setSpawn(Spawn spawn) {
        this.spawn = spawn;
    }

    public void run() {
        try {
            addQueue();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private synchronized void addQueue() {
        try {
            getSpawn().addToQueue(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Bus(Spawn spawn) {

        this.spawn = spawn;
    }

    public long getBusTimeIn() {
        return busTimeIn;
    }

    public void setBusTimeIn(long busTimeIn) {
        this.busTimeIn = busTimeIn;
    }

    public long getBusTimeOut() {
        return busTimeOut;
    }

    public void setBusTimeOut(long busTimeOut) {
        this.busTimeOut = busTimeOut;
    }

}

class Minibus extends Bus {

    private int size = 1;

    Minibus(Spawn spawn) {
        super(spawn);
    }
    
    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

}
