/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccp;

import java.util.LinkedList;

/**
 *
 * @author kklee
 */
class Bay extends Thread {

    private int numberOfBays = 5;
    private volatile boolean closingTime = false;
    private Depot depot;
    private MBay mbay;
    private CBay cbay;

    public Bay(Depot depot, MBay mbay, CBay cbay) {
        this.depot = depot;
        this.mbay = mbay;
        this.cbay = cbay;

    }

    @Override
    public void run() {
        this.setName("Bay-Thread");
        try {
            createMBay();
            createCBay();
            while (!closingTime) {
                getDepot().enterMechanicBay();

            }
            if (closingTime) {
                try {
                    this.sleep(1000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void createMBay() {

        int x = 0;

        while (getNumberOfBays() > x) {

            MBay mbay = new MBay(depot, cbay, true);
            mbay.setmBayName("Mechanic Bay" + x);
            ((LinkedList<MBay>) getDepot().getmBayQ()).offer(mbay);
            // System.out.println(mbay.getmBayName());
            Thread m = new Thread(mbay);
            m.start();

            x += 1;
        }
    }

    public void createCBay() {

        int x = 0;

        while (getNumberOfBays() > x) {

            CBay cbay = new CBay(depot);
            cbay.setcBayName("Cleaning Bay" + x);
            ((LinkedList<CBay>) getDepot().getcBayQ()).offer(cbay);
            // System.out.println(cbay.getcBayName());
            Thread c = new Thread(cbay);
            c.start();

            x += 1;
        }
    }

    public synchronized void setClosingTime() {
        setClosingTime(true);
        System.out.println("\nBAY IS CLOSED! NO MORE BUS\n");
    }

    /**
     * @return the numberOfBays
     */
    public int getNumberOfBays() {
        return numberOfBays;
    }

    /**
     * @param numberOfBays the numberOfBays to set
     */
    public void setNumberOfBays(int numberOfBays) {
        this.numberOfBays = numberOfBays;
    }

    /**
     * @return the closingTime
     */
    public boolean isClosingTime() {
        return closingTime;
    }

    /**
     * @param closingTime the closingTime to set
     */
    public void setClosingTime(boolean closingTime) {
        this.closingTime = closingTime;
    }

    /**
     * @return the depot
     */
    public Depot getDepot() {
        return depot;
    }

    /**
     * @param depot the depot to set
     */
    public void setDepot(Depot depot) {
        this.depot = depot;
    }

    /**
     * @return the mbay
     */
    public MBay getMbay() {
        return mbay;
    }

    /**
     * @param mbay the mbay to set
     */
    public void setMbay(MBay mbay) {
        this.mbay = mbay;
    }

    /**
     * @return the cbay
     */
    public CBay getCbay() {
        return cbay;
    }

    /**
     * @param cbay the cbay to set
     */
    public void setCbay(CBay cbay) {
        this.cbay = cbay;
    }

}
