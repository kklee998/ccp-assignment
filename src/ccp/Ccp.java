/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccp;

/**
 *
 * @author kklee
 */
public class Ccp {

    public static void main(String[] args) {

        System.out.println("hello world!");

        Spawn spawn = new Spawn(100, true);
        Depot depot = new Depot(spawn);
        Ramp ramp = new Ramp(depot);
        WaitingArea wa = new WaitingArea(depot);
        Bus bus = new Bus(spawn);

        CBay cbay = new CBay(depot);
        MBay mbay = new MBay(depot, cbay, false);
        Bay bay = new Bay(depot, mbay, cbay);
        Mechanic mech = new Mechanic(mbay, depot);
        Cleaner clean = new Cleaner(cbay, depot);
        

        Clock clock = new Clock(bus, spawn, ramp, wa, bay, mech, clean, depot, mbay);
        clock.start();
        spawn.start();
        ramp.start();
        wa.start();
        bay.start();

    }

}
