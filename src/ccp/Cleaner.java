/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccp;

/**
 *
 * @author kklee
 */
public class Cleaner extends Thread {

    private Depot depot;
    private CBay cbay;
    private volatile boolean atWork = true;

    public Cleaner(CBay cbay, Depot depot) {
        this.cbay = cbay;
        this.depot = depot;
    }

    public void run() {
        this.setName("Cleaner " + this.getId());
        while (!isAtWork()) {
            getCbay().cleanBus(this);
            try {
                sleep(100);
            }catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void goHome() {
        setAtWork(false);
        System.out.println("CLeaners will go home now");

    }

    /**
     * @return the depot
     */
    public Depot getDepot() {
        return depot;
    }

    /**
     * @param depot the depot to set
     */
    public void setDepot(Depot depot) {
        this.depot = depot;
    }

    /**
     * @return the cbay
     */
    public CBay getCbay() {
        return cbay;
    }

    /**
     * @param cbay the cbay to set
     */
    public void setCbay(CBay cbay) {
        this.cbay = cbay;
    }

    /**
     * @return the atWork
     */
    public boolean isAtWork() {
        return depot.closingTime;
    }

    /**
     * @param atWork the atWork to set
     */
    public void setAtWork(boolean atWork) {
        this.atWork = atWork;
    }

}
