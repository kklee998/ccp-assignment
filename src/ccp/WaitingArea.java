/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccp;

/**
 *
 * @author kklee
 */
public class WaitingArea extends Thread {

    private volatile boolean closingTime = false;
    private Depot depot;

    public WaitingArea(Depot depot) {
        this.depot = depot;
    }

    public void run() {
        this.setName("WaitingArea-Thread");
        while (!isClosingTime()) {
            try {
                getDepot().enterEntrance();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void setClosingTime() {
        setClosingTime(true);
        System.out.println("\nWaiting Area is closing!\n");
    }

    /**
     * @return the closingTime
     */
    public boolean isClosingTime() {
        return closingTime;
    }

    /**
     * @param closingTime the closingTime to set
     */
    public void setClosingTime(boolean closingTime) {
        this.closingTime = closingTime;
    }

    /**
     * @return the depot
     */
    public Depot getDepot() {
        return depot;
    }

    /**
     * @param depot the depot to set
     */
    public void setDepot(Depot depot) {
        this.depot = depot;
    }
}
