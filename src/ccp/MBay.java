/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccp;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author kklee
 */
public class MBay implements Runnable {

    private String mBayName;
    private Bus bus;
    private List<Bus> busInMBay;
    private Bay bay;
    private Depot depot;
    CBay cbay;
    Mechanic mech;
    int repairTime = 500;
    int superRepairTime = 500;
    public volatile boolean atWork = true;

    public MBay(Depot depot, CBay cbay, boolean flag) {
        busInMBay = new LinkedList<>();

        this.depot = depot;
        this.cbay = cbay;

    }
    
    @Override
    public void run() {
        Mechanic m;
        m = new Mechanic(this, depot);
        m.start();
    }

    synchronized public void addToBusInMBay(Bus bus) {

        synchronized (busInMBay) {

            ((LinkedList<Bus>) getBusInMBay()).offer(bus);
            System.out.println("\n" + bus.getName() + " is added into mechanic bay: "
                    + getmBayName() + "\n");
        }
    }

    public void fixBus(Mechanic mech) {
        synchronized (busInMBay) {
            bus = ((LinkedList<Bus>) busInMBay).poll();
        }

        try {
            //System.out.println(bus.getName());
            if (bus != null) {
                if (bus.isNeedRepair()) {
                    System.out.println(bus.getName() + "is now being "
                            + "repair by " + mech.getName() + " in "
                            + this.getmBayName());
                    bus.setNeedRepair(false);
                    Thread.sleep(repairTime * bus.getSize());

                }
                if (bus.isNeedExtraRepair()) {
                    System.out.println(bus.getName() + "is now being "
                            + "SUPER repair by " + mech.getName() + " in "
                            + this.getmBayName());
                    bus.setNeedExtraRepair(false);
                    Thread.sleep(superRepairTime * bus.getSize());
                }

                System.out.println("\n" + bus.getName() + " REPAIRS DONE!\n");

                if (bus.isNeedClean()) {
                    depot.enterCleanerBay(bus);

                } else {
                    depot.enterExit(bus);
                }
            }
            synchronized (depot.getmBayQ()) {
                ((LinkedList) depot.getmBayQ()).offer(this);
            }

            //}//END ITER
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //}
    }

//    public void goHome() {
//        busInMBay.notifyAll();
//    } //not functioning
    public void setClosing() {
        atWork = false;
    }

    /**
     * @return the mBayName
     */
    public String getmBayName() {
        return mBayName;
    }

    /**
     * @param mBayName the mBayName to set
     */
    public void setmBayName(String mBayName) {
        this.mBayName = mBayName;
    }

    /**
     * @return the bus
     */
    public Bus getBus() {
        return bus;
    }

    /**
     * @param bus the bus to set
     */
    public void setBus(Bus bus) {
        this.bus = bus;
    }

    /**
     * @return the busInMBay
     */
    public List<Bus> getBusInMBay() {
        return busInMBay;
    }

    /**
     * @param busInMBay the busInMBay to set
     */
    public void setBusInMBay(List<Bus> busInMBay) {
        this.busInMBay = busInMBay;
    }

    /**
     * @return the bay
     */
    public Bay getBay() {
        return bay;
    }

    /**
     * @param bay the bay to set
     */
    public void setBay(Bay bay) {
        this.bay = bay;
    }

    /**
     * @return the depot
     */
    public Depot getDepot() {
        return depot;
    }

    /**
     * @param depot the depot to set
     */
    public void setDepot(Depot depot) {
        this.depot = depot;
    }

}
