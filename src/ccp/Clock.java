package ccp;

/**
 *
 * @author kklee
 *
 * Time Reference:
 *
 * Opening to closing time: 120000ms (2 minutes, real-life time: 8 hours) Ramp
 * time: 125ms (0.125 seconds, real-life time: 30 seconds) Cleaning: 7500ms (7.5
 * seconds, real-life time: 30 minutes) Repair: 7500 ms (7.5 seconds, real-life
 * time: 30 minutes)
 *
 */
public class Clock extends Thread {

    private int counter = 0;
    private int closingTime = 26;
    private int beforeClosing = closingTime - 2;

    private Spawn spawn;
    private Ramp ramp;
    private WaitingArea wa;
    Bay bay;
    Bus bus;
    Depot depot;
    Mechanic mech;
    Cleaner clean;
    MBay mbay;

    Clock(Bus bus, Spawn spawn, Ramp ramp, WaitingArea wa, Bay bay, Mechanic mech, Cleaner clean, Depot depot, MBay mbay) {
        this.bus = bus;
        this.spawn = spawn;
        this.ramp = ramp;
        this.wa = wa;
        this.bay = bay;
        this.mech = mech;
        this.clean = clean;
        this.depot = depot;
        this.mbay = mbay;
    }

    @Override
    public void run() {
        this.setName("Clock-Thread");
        while (true) {
            if (getCounter() < 32) {
                
                //end of entire program
                System.out.println("CLOCK: TIME IS NOW=> " + getCounter() + "\n");

                try {
                    Thread.sleep(1000);

                } catch (InterruptedException e) {
                    System.err.println(e);
                }
                setCounter(getCounter() + 1);

            }

            //30 minutes before closing (rounded up)
            if (getCounter() == getBeforeClosing()) {
                System.out.println("30 MINUTES BEFORE CLOSING!!");
                getRamp().setBeforeClosingTime();
                getSpawn().setBeforeClosingTime();

            }
            
            //closing time

            if (getCounter() == getClosingTime()) {
                System.out.println("CLOCK: IT'S CLOSING TIME!\n\n");


                getSpawn().setClosingTime();
                getRamp().setClosingTime();
                getWa().setClosingTime();
                bay.setClosingTime();
                depot.setClosingTime();
                mbay.setClosing();
                System.out.println(mbay.atWork);

            }

            if (getCounter() == 30) {
                
                System.out.println("end");
                mech.goHome();
                clean.goHome();
                depot.depotStats();

            }
            
            if (getCounter() == 31) {
                return;
            }

        }

    }

    /**
     * @return the counter
     */
    public int getCounter() {
        return counter;
    }

    /**
     * @param counter the counter to set
     */
    public void setCounter(int counter) {
        this.counter = counter;
    }

    /**
     * @return the closingTime
     */
    public int getClosingTime() {
        return closingTime;
    }

    /**
     * @param closingTime the closingTime to set
     */
    public void setClosingTime(int closingTime) {
        this.closingTime = closingTime;
    }

    /**
     * @return the beforeClosing
     */
    public int getBeforeClosing() {
        return beforeClosing;
    }

    /**
     * @param beforeClosing the beforeClosing to set
     */
    public void setBeforeClosing(int beforeClosing) {
        this.beforeClosing = beforeClosing;
    }

    /**
     * @return the spawn
     */
    public Spawn getSpawn() {
        return spawn;
    }

    /**
     * @param spawn the spawn to set
     */
    public void setSpawn(Spawn spawn) {
        this.spawn = spawn;
    }

    /**
     * @return the ramp
     */
    public Ramp getRamp() {
        return ramp;
    }

    /**
     * @param ramp the ramp to set
     */
    public void setRamp(Ramp ramp) {
        this.ramp = ramp;
    }

    /**
     * @return the wa
     */
    public WaitingArea getWa() {
        return wa;
    }

    /**
     * @param wa the wa to set
     */
    public void setWa(WaitingArea wa) {
        this.wa = wa;
    }

}
